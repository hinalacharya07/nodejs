const mongoose = require('mongoose');
const studentSchema = new mongoose.Schema({
    rno : {
        type :Number
    },
    name: {
        type : String
    },
    
    dept: {
        type : String
    },
    sem: {
        type : Number
    },
    div: {
        type : String
    }
});

module.exports = mongoose.model('students',studentSchema);
