//const { json } = require('express');
var express = require('express');
var mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const studentModel = require('./model/studentModel');
const tamp = mongoose.model('students');
const User = require('./model/user');
const cookieParser = require('cookie-parser');
var app = express();
app.use(cookieParser());
app.set('view engine','ejs');
app.listen(8000);

mongoose.connect('mongodb://localhost:27017/test',{
    useNewUrlParser: true,
      useUnifiedTopology: true,
})
.then(() => {
    console.log("Successfully connected to database");
})

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.get('/', function(req,res){
    res.send("Welcome to Student Dashboard. <br> <a href='/login'>LogIn</a>");
});

app.get('/add',function(req,res){
    res.render("add");
});

app.post('/add',function(req,res){
    console.log(req.body);
    studentModel.insertMany(req.body)
    .then(result => {
        res.send(result);
        res.redirect("/dashboard");
    })
    .catch(error => console.log(error))
});

app.get('/edit/:id',function(req,res){
     studentModel.findOne({_id : req.params.id},(err,doc)=>{
         if(err)
            console.log(err);
        else
        {
            res.send(doc);
            res.render('edit',{details : doc});
        }
    })
    })
app.post('/edit', function(req,res){
    studentModel.findByIdAndUpdate({_id: req.body._id}, {name : req.body.name ,dept : req.body.dept, sem : req.body.sem,div : req.body.div} ,function(err,doc){
            if(err)
                console.log(err);
            else
            {
                res.send(doc);
                res.redirect('/dashboard');
            }
            })
        })
        
app.get('/delete/:id', function(req, res) {
    tamp.findByIdAndRemove(req.params.id, function(err,doc)
    {
        if(err)
            console.log(err);
            
        else
            {res.send(doc);
            res.redirect('/dashboard');}
     });
});

app.get('/dashboard', function(req, res, next) {
    studentModel.find({},(err,docs)=>{
        if(err)
        {
            console.log(err);
        }
        else{
            res.json(docs);
            res.render('dashboard',{details : docs});
        }
      }) ;
  });

app.get('/login',function(req,res){
    res.render('login');
});

app.post('/validate',async function(req,res){
    try{
        var mail = req.body.email;
        var pass = req.body.password;
       // console.log(req.body.password);
       const data = await User.findOne({email : req.body.email});
       if(data)
       {
           console.log(data.email);
           if(pass == data.password)
            {
                let _token = jwt.sign({email : data.email},
                    "secretkey2001",{
                        expiresIn : 86400,
                    });
                res.cookie("jwt", _token,{
                    secure : false,httpOnly:true});
                res.redirect("/dashboard");
            }
            else
                res.send("Invalid password");
       }
       else 
            res.send("Invalid Email");
    }

    catch(err)
    {
        console.log(err);
    }
});

app.get('/logout', function(req,res){
    res.clearCookie("jwt");
  res.redirect("/");
});