const express = require('express');
const session = require('express-session')
const bodyParser = require('body-parser')
const path = require('path')
const app = express();

app.use(session({secret:'enc-key',name:'uniqueSessionID',saveUninitialized:false}));

app.get('/',function(req,res){
        if(req.session.loggedIn)
            res.redirect('/dashboard')
        else
            res.sendFile('login.html',{root:path.join(__dirname,'public')})
        });

app.get('/dashboard',(req,res)=>
{
    if(req.session.loggedIn) 
    {
        res.setHeader('Content-Type','text/html')
        res.write('Welcome '+req.session.username+' to your dashboard')
        res.write('<a href="/logout">Logout</a>')
        res.end()
    }
    else
        res.redirect('/login')
});

app.get('/login',(req,res)=>
{
    res.sendFile('login.html',{root:path.join(__dirname,'public')})
});

    app.post('/authenticate',bodyParser.urlencoded(),(req,res,next)=>{

    if(req.body.username=='foo'&&req.body.password=='bar') 
    {
        res.locals.username = req.body.username
        next();
    }
    else    
        res.send("Please enter valid username and password");
}
,(req,res)=>
{
    req.session.loggedIn = true
    req.session.username = res.locals.username
    console.log(req.session)
    res.redirect('/dashboard')
})

app.get('/logout',(req,res)=>
{
    req.session.destroy((err)=>{})
    res.send('Thank you! Visit again')
})
app.listen(8000);