var express = require('express');
var multer = require('multer');
var path = require('path');
var app = express();

app.use(express.static('public'));

var option = multer.diskStorage({destination: function(req,file,cb){

    if(file.mimetype !== 'image/jpeg')
    {
        return cb('Invalid file formate');
    }
    cb(null, './uploads');

} ,filename: function(req,file,cb){

    cb(null, (Math.random.toString(30)).slice(5,10) + Date.now() + path.extname(file.originalname));

}});
var upload = multer({storage: option});

app.get('/', function(req,res){
    res.send("<a href = 'file_upload.html'> Click here to upload files </a>");
});

app.post('/file_upload', upload.single('myfile') ,function(req,res){
    res.send("file uploded by " + req.body.fname);
});

app.post('/photos_upload', upload.array('photos',2) ,function(req,res){
    res.send("file uploded by " + req.body.fname);
});
app.listen(8000);
