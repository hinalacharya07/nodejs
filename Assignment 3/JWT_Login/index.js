const express = require('express');
const app = express();
const mongo = require('mongoose');
//const userModel = require('./model/user');
const loginController = require('./controller/loginController');

mongo.connect('mongodb://localhost:27017/test',{
    useNewUrlParser : true})
    .then(()=> {console.log("Database is connected")})
    .catch(()=>{console.log("Error in Database connection")});
    
    app.set('view engine','ejs');
    app.use(express.json());
    app.use(express.urlencoded({extended:true}));
    
    loginController(app);
    app.listen('8080',()=>{console.log("Port : 8080")})