const userModel = require('../model/user');
const regModel = require('../model/reg');
const jwt = require('jsonwebtoken');
const multer = require('multer');
const path = require('path');
const fs = require('fs');
const { verify } = require("./middleware/auth");

const {check ,validationResult} = require('express-validator');

module.exports = function(app){

    app.get('/',(req,res)=>{
        res.send("Welcome <br> <a href='/login'> Login </a>");
    });

    app.get('/login',(req,res)=>{
        res.render('login');
    });

    app.post('/jwtlogin',async function(req,res){
       const data = await userModel.findOne({email : req.body.email});
       if(data)
       {
            if(req.body.password == data.password)
            {
                let _token = jwt.sign({email : data.email},
                    'secretKey2001',{
                    expiresIn:"1h"
                });
                res.cookie("jwt",_token,{
                    httpOnly : true,
                    secure : false
                });
             res.redirect('/display');
            }
            else
                res.send("Incorrect Password");
       }
       else
        res.send("You are not registerd");
    });

    var option = multer.diskStorage({destination: function(re,file,cb){
        if(file.mimetype != "image/jpeg")
            {
                return cb("Invalid type");
            }
            cb(null,'../uploads');

        },filename: function(req,file,cb){
            cb(null,file.fieldname + Date.now());
        }
    });
    var upload = multer({storage : option});

    app.post('/insert',[check('name',"Name is required").isEmpty()],upload.single('img') ,(req,res,file)=>{
        //var file = req.file;
        //var uploadFile = req.file.;
        const errors = validationResult(req);
        var sub="";
        if(!errors.isEmpty())
        {
            res.send(errors);
        }
        else
        {
           // res.send("no error");
         if(Array.isArray(req.body.subject))
            {sub = "asp,node";}

        else
            { sub = req.body.subject;}
           // res.send(uploadFile);
            regModel.insertMany({name :req.body.fname, gender : req.body.gender, city: req.body.city, subject :sub, img: req.file.filename },
                function(err,doc){
                        if(err)
                            console.log(err)
                        else
                            res.redirect('/display');
            });
         }
   
     });
    
     app.get('/display',verify,(req,res)=>{

        regModel.find({},function(err,doc){
            if(err)
                console.log(err);
            else
            {
                res.render('display',{details : doc});
            }
        })
     });

     app.get('/delete/:id',function(req,res){

        regModel.findByIdAndDelete(req.params.id,(err,doc)=>{

            if(err)
                console.log(err);
            else
                res.redirect('/display');
        });
     });

     app.get('/edit/:id',function(req,res){

        regModel.findById({_id : req.params.id},(err,doc)=>{

            if(err)
                console.log(err);
            else
                res.render('edit',{details : doc})
        })
     });

     app.post('/edit',function(req,res){

        var sub="";
        if(Array.isArray(req.body.subject))
        {
            sub = "asp,node";
        }
        else
           { sub = req.body.subject;}

         regModel.findByIdAndUpdate({_id : req.body._id},{name: req.body.fname , gender : req.body.gender, city : req.body.city ,subject : sub , img : req.body.img },function(err,doc){
             if(err)
                console.log(err);
            else
                res.redirect('/display');
         })
     });

     app.post('/search',(req,res)=>{

        regModel.find({name : req.body.searchData},(err,doc)=>{
                
                if(err)
                    console.log(err);
                else
                    res.render('display',{details : doc})
        })
     })
}