const mongo = require('mongoose');
const regModel = mongo.Schema({
    name :{type :String},
    gender:{type :String},
    city:{type :String},
    subject:{type :String},
    img:{type :String},
});

module.exports = mongo.model('reg',regModel);